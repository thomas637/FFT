#include "../include/mpi_fft.h"

int curvyToFlat(int* indices, int* sizes, int dimension)
{
    int rowMajorIndex = 0;

    for (int i = 0; i < dimension; i++) {
        rowMajorIndex *= sizes[i];
        rowMajorIndex += indices[i];
    }

    return rowMajorIndex;
}

void flatToCurvy(int* indices, int* sizes, int dimension, int flatIndex)
{
    for (int i = dimension - 1; i >= 0; i--) {
        indices[i] = flatIndex % sizes[i];
        flatIndex /= sizes[i];
    }
}

void tableInitialization(complex double* table, int n, int p, int s)
{
    for (int j = 0; j < n/p; j++) {
        table[j] = cexp(-2.0 * M_PI * j * s * I / n);
    }
}

void tablesInitialization(complex double** tables, int n[], int p[], int s[], int dimension)
{
    for (int i = 0; i < dimension; i++) {
        tableInitialization(tables[i], n[i], p[i], s[i]);
    }
}

void displacementInit(int* rdispls, int dimension, int n[], int p[], int index, int depth, int sum)
{
    // Recursive part
    if (depth < dimension - 1) {
        int bigStride = 1; // For the n/p array
        int smallStride = 1; // For the p array
        for (int i = depth + 1; i < dimension; i++) {
            bigStride *= n[i]/p[i];
            smallStride *= p[i];
        }

        for (int sl = 0; sl < p[depth]; sl++) {
            displacementInit(rdispls, dimension, n, p, index, depth + 1, sum + n[depth] / p[depth] / p[depth] * bigStride * sl);
            index += smallStride;
        }
    }

    // Iterative part
    else {
        for (int s = 0; s < p[dimension - 1]; s++) {
            rdispls[index] = sum + n[dimension - 1] / p[dimension - 1] / p[dimension - 1] * s;
            index++;
        }
    }
}

void packAndTwiddle(complex double* localData, complex double* sourceBuffer, int n[], int p[],
                    int localDataIndex, int packetIndex, int m, int depth, int dimension, complex double factor,
                    complex double** tables, int elementsPerPacket)
{
    // Recursive part
    if (depth < dimension - 1) {
        int localDataStride = 1;
        int packetStride = 1;
        int mStride = 1;
        for (int l = depth + 1; l < dimension; l++) {
            localDataStride *= n[l]/p[l];
            packetStride *= n[l]/p[l]/p[l];
            mStride *= p[l];
        }

        for (int t = 0; t < n[depth]/p[depth]; ) {
            for (int k = 0; k < p[depth]; k++) {
                packAndTwiddle(localData, sourceBuffer, n, p, localDataIndex, packetIndex, m + k * mStride,
                depth + 1, dimension, factor * tables[depth][t], tables, elementsPerPacket);
                t++;
                localDataIndex += localDataStride;
            }
            packetIndex += packetStride;
        }
    }

    // Iterative part
    else {
        for (int t = 0; t < n[dimension - 1]/p[dimension - 1];) {
            for (int k = 0; k < p[dimension - 1]; k++) {
                sourceBuffer[(m + k) * elementsPerPacket + packetIndex] = localData[localDataIndex] * factor
                * tables[dimension - 1][t];
                t++;
                localDataIndex++;
            }
            packetIndex++;
        }
    }
}

void unpack(complex double* localData, complex double* packet, int packetDimensions[], int localDataDimensions[],
                    long localDataIndex, long packetIndex, int depth, int dimension)
{
    // Recursive part
    if (depth < dimension - 1) {
        long localDataStride = 1;
        long packetStride = 1;
        for (int l = depth + 1; l < dimension; l++) {
            localDataStride *= localDataDimensions[l];
            packetStride *= packetDimensions[l];

        }
        for (int t = 0; t < packetDimensions[depth]; t++) {
            unpack(localData, packet, packetDimensions, localDataDimensions, localDataIndex, packetIndex, depth + 1, dimension);
            localDataIndex += localDataStride;
            packetIndex += packetStride;
        }
    }

    // Iterative part
    else {
        memcpy(localData + localDataIndex, packet + packetIndex, sizeof(complex double) * packetDimensions[dimension - 1]);
    }
}

void mpiRedistributeAndTwiddle(complex double* localData, int dimension, int n[], int p[])
{
    // Preperation: allocating memory, initializing stuff, etc.
    // ---------------------------------------------------------------------------------------
    int s;
    MPI_Comm_rank(MPI_COMM_WORLD, &s);

    int totalNumberOfElements = 1;
    int totalProcessors = 1;

    for (int i = 0; i < dimension; i++) {
        totalNumberOfElements *= n[i]/p[i];
        totalProcessors *= p[i];
    }

    complex double* sourceBuffer = malloc(totalNumberOfElements * sizeof(complex double));

    if (sourceBuffer == NULL) {
        printf("\nInsufficient memory, aborted.\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    int* sendcounts = malloc(totalProcessors * sizeof(int));
    int* sdispls = malloc(totalProcessors * sizeof(int));
    int* recvcounts = malloc(totalProcessors * sizeof(int));
    int* rdispls = malloc(totalProcessors * sizeof(int));
    int zero[dimension]; // zeroes (for initializing the subarray type)
    int array_of_subsizes[dimension]; // sizes of the subarrays sent to different processes
    int np[dimension]; // local data sizes
    int sMulti[dimension]; // the non-row-major format of this processor id

    displacementInit(rdispls, dimension, n, p, 0, 0, 0);

    flatToCurvy(sMulti, p, dimension, s);

    complex double** tables = malloc(dimension * sizeof(void*));

    for (int i = 0; i < dimension; i++) {
        tables[i] = malloc(n[i]/p[i] * sizeof(complex double));
        np[i] = n[i]/p[i];
        array_of_subsizes[i] = np[i]/p[i];
        zero[i] = 0;
    }

    int elementsPerPackage = totalNumberOfElements / totalProcessors;
    tablesInitialization(tables, n, p, sMulti, dimension);

    for (int i = 0; i < totalProcessors; i++) {
        sendcounts[i] = elementsPerPackage;
        sdispls[i] = i * elementsPerPackage;
        recvcounts[i] = 1;
    }

    MPI_Datatype subarray;
    MPI_Type_create_subarray(dimension, np, array_of_subsizes, zero, MPI_ORDER_C,
    MPI_C_DOUBLE_COMPLEX, &subarray);

    MPI_Datatype subarrayResized; // So we can give the displacements in elements instead of bytes.
    MPI_Type_create_resized(subarray, 0, sizeof(complex double), &subarrayResized);
    MPI_Type_commit(&subarrayResized);

    // The packing, twiddling and sending of the packets.
    // ---------------------------------------------------------------------------------------
    packAndTwiddle(localData, sourceBuffer, n, p, 0, 0, 0, 0, dimension, 1, tables, elementsPerPackage);

    MPI_Barrier(MPI_COMM_WORLD); // In most implementations like MPICH and openMPI, MPI_Alltoallv is synchronizing, but
                                 // this is not required by the standard.

    MPI_Alltoallv(sourceBuffer, sendcounts, sdispls, MPI_C_DOUBLE_COMPLEX, localData,
    recvcounts, rdispls, subarrayResized, MPI_COMM_WORLD);

    // Cleanup
    // ---------------------------------------------------------------------------------------
    free(sourceBuffer);
    for (int i = 0; i < dimension; i++) {
        free(tables[i]);
    }
    free(tables);
    free(sendcounts);
    free(sdispls);
    free(recvcounts);
    free(rdispls);
}

void mpiRedistributeAndTwiddle2(complex double* localData, int dimension, int n[], int p[])
{
    // Preperation: allocating memory, initializing stuff, etc.
    // ---------------------------------------------------------------------------------------
    int s;
    MPI_Comm_rank(MPI_COMM_WORLD, &s);

    int totalNumberOfElements = 1;
    int totalProcessors = 1;
    int packetDimensions[dimension];
    int localDataDimensions[dimension];
    int sMulti[dimension];
    int kMulti[dimension];
    int startingPoint[dimension];

    for (int i = 0; i < dimension; i++) {
        totalNumberOfElements *= n[i]/p[i];
        totalProcessors *= p[i];
        packetDimensions[i] = n[i]/p[i]/p[i];
        localDataDimensions[i] = n[i]/p[i];
    }

    complex double* buffer = malloc(totalNumberOfElements * sizeof(complex double));

    if (buffer == NULL) {
        printf("\nInsufficient memory, aborted.\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    flatToCurvy(sMulti, p, dimension, s);

    complex double** tables = malloc(dimension * sizeof(void*));

    for (int i = 0; i < dimension; i++) {
        tables[i] = malloc(n[i]/p[i] * sizeof(complex double));
    }

    int elementsPerPackage = totalNumberOfElements / totalProcessors;
    tablesInitialization(tables, n, p, sMulti, dimension);

    // The packing, twiddling and sending of the packets.
    // ---------------------------------------------------------------------------------------
    packAndTwiddle(localData, buffer, n, p, 0, 0, 0, 0, dimension, 1, tables, elementsPerPackage);

    MPI_Barrier(MPI_COMM_WORLD);

    MPI_Alltoall(MPI_IN_PLACE, totalNumberOfElements / totalProcessors, MPI_C_DOUBLE_COMPLEX, buffer,
    totalNumberOfElements / totalProcessors, MPI_C_DOUBLE_COMPLEX, MPI_COMM_WORLD);

    // Unpacking
    for (int k = 0; k < totalProcessors; k++) {
        flatToCurvy(kMulti, p, dimension, k);
        for (int i = 0; i < dimension; i++) {
            startingPoint[i] = kMulti[i] * packetDimensions[i];
        }
        unpack(localData, buffer + k * elementsPerPackage, packetDimensions, localDataDimensions, curvyToFlat(startingPoint, localDataDimensions, dimension),
        0, 0, dimension);
    }

    // Cleanup
    // ---------------------------------------------------------------------------------------
    free(buffer);
    for (int i = 0; i < dimension; i++) {
        free(tables[i]);
    }
    free(tables);
}



void fftu(complex double* localData, int dimension, int n[], int p[], fftu_plan plan)
{
    fftw_execute(plan.bigPlan);

    mpiRedistributeAndTwiddle(localData, dimension, n, p);

    fftw_execute(plan.manyPlans);
}

// ---------------------------------------------------------------------------------------------------------------
// We have now fully implemented the parallel fft function fftu. Creating the plans fftu takes as argument,
// and determining the number of processors you can use is not trivial. So below is some code taking care of this.
// Hopefully this makes using fftu less painful.

fftw_plan createManyPlans(complex double* localData, int dimension, int n[], int p[], unsigned flag)
{
    bool validInput = true;
    for (int i = 0; i < dimension; i++) {
        if (n[i] % (p[i] * p[i]) != 0) {
            validInput = false;
        }
    }

    if (!validInput) {
        printf("\nPlease make sure that p_i^2 divides n_i in every dimension.\n");
    }

    fftw_iodim* sizes = malloc(dimension * sizeof(fftw_iodim));
    fftw_iodim* startingPoints = malloc(dimension * sizeof(fftw_iodim));

    int strideFactor = 1; // will be updated in the loop, at iteration i it should be
                           // n[i + 1] ... n[dimension - 1] (empty product is 1)
    for (int i = dimension - 1; i >= 0; i--) {
        sizes[i].n = p[i];
        sizes[i].is = n[i]/p[i]/p[i] * strideFactor;
        sizes[i].os = sizes[i].is;

        startingPoints[i].n = n[i]/p[i]/p[i];
        startingPoints[i].is = strideFactor;
        startingPoints[i].os = startingPoints[i].is;

        strideFactor *= n[i]/p[i];
    }

    fftw_plan manyPlans = fftw_plan_guru_dft(dimension, sizes, dimension, startingPoints, localData, localData, FFTW_FORWARD, flag);

    free(sizes);
    free(startingPoints);

    return manyPlans;
}

fftw_plan createBigPlan(complex double* localData, int dimension, int n[], int p[], unsigned flag)
{
    bool validInput = true;
    for (int i = 0; i < dimension; i++) {
        if (n[i] % (p[i] * p[i]) != 0) {
            validInput = false;
        }
    }

    if (!validInput) {
        printf("\nPlease make sure that p_i^2 divides n_i in every dimension.\n");
    }

    int* np = malloc(dimension * sizeof(int));
    for (int i = 0; i < dimension; i++) {
        np[i] = n[i]/p[i];
    }
    fftw_plan plan = fftw_plan_dft(dimension, np, localData, localData, FFTW_FORWARD, flag);
    free(np);
    return plan;
}

fftu_plan createPlan(complex double* localData, int dimension, int n[], int p[], unsigned flag)
{
    fftu_plan plan;

    plan.bigPlan = createBigPlan(localData, dimension, n, p, flag);
    plan.manyPlans = createManyPlans(localData, dimension, n, p, flag);

    return plan;
}

void fftu_destroy_plan(fftu_plan plan)
{
    fftw_destroy_plan(plan.bigPlan);
    fftw_destroy_plan(plan.manyPlans);
}

int largestSquareDivisor(int n, int p)
{
    int largestSquareDivisor = 1;
    int candidate = 1;

    while (candidate * candidate <= n) {
        if ((n % (candidate * candidate) == 0) && (p % candidate == 0)) {
            largestSquareDivisor = candidate;
        }
        candidate++;
    }

    return largestSquareDivisor;
}

bool checkAdmissable(int n[], int p, int d)
{
    for (int l = 0; l < d; l++) {
        p /= largestSquareDivisor(n[l], p);
    }

    return p == 1;
}

bool fillProcessors(int n[], int p, int* pMulti, int d)
{
    for (int l = 0; l < d; l++) {
        pMulti[l] = largestSquareDivisor(n[l], p);
        p /= pMulti[l];
    }

    return p == 1;
}

int fftu_malloc(int n[], int p[], int d, complex double **localElements)
{
    int total_elem = 1;
    for (int i = 0; i < d; i++) {
        total_elem *= (n[i] / p[i]);
    }
    *localElements = fftw_malloc(total_elem * sizeof(complex double));
    return total_elem;
}

void printAdmissableProcessors(int n[], int d)
{
    printf("\n");

    int nTotal = 1;
    for (int i = 0; i < d; i++) {
        nTotal *= n[i];
    }

    // if p factorizes like that, p^2 <= n1 ... nd
    for (int p = 0; p <= sqrt(nTotal); p++) {
        if (checkAdmissable(n, p, d)) {
            printf("%d, ", p);
        }
    }

    printf("\n");
}
