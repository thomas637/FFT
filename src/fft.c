#include "../include/fft.h"

long curvyToFlat(int* indices, int* sizes, int dimension)
{
    long rowMajorIndex = 0;

    for (int i = 0; i < dimension; i++) {
        rowMajorIndex *= sizes[i];
        rowMajorIndex += indices[i];
    }

    return rowMajorIndex;
}

void flatToCurvy(int* indices, int* sizes, int dimension, int flatIndex)
{
    for (int i = dimension - 1; i >= 0; i--) {
        indices[i] = flatIndex % sizes[i];
        flatIndex /= sizes[i];
    }
}

void tableInitialization(complex double* table, int n, int p, int s)
{
    for (int j = 0; j < n/p; j++) {
        table[j] = cexp(-2.0 * M_PI * j * s * I / n);
    }
}

void tablesInitialization(complex double** tables, int n[], int p[], int s[], int dimension)
{
    for (int i = 0; i < dimension; i++) {
        tableInitialization(tables[i], n[i], p[i], s[i]);
    }
}

void packAndTwiddle(complex double* localData, complex double* sourceBuffer, int n[], int p[],
                    long localDataIndex, long packetIndex, int m, int depth, int dimension, complex double factor,
                    complex double** tables, long elementsPerPacket)
{
    // Recursive part
    if (depth < dimension - 1) {
        long localDataStride = 1;
        long packetStride = 1;
        long mStride = 1;
        for (int l = depth + 1; l < dimension; l++) {
            localDataStride *= n[l]/p[l];
            packetStride *= n[l]/p[l]/p[l];
            mStride *= p[l];
        }

        for (int t = 0; t < n[depth]/p[depth]; ) {
            for (int k = 0; k < p[depth]; k++) {
                packAndTwiddle(localData, sourceBuffer, n, p, localDataIndex, packetIndex, m + k * mStride,
                depth + 1, dimension, factor * tables[depth][t], tables, elementsPerPacket);
                t++;
                localDataIndex += localDataStride;
            }
            packetIndex += packetStride;
        }
    }

    // Iterative part
    else {
        for (int t = 0; t < n[dimension - 1]/p[dimension - 1];) {
            for (int k = 0; k < p[dimension - 1]; k++) {
                sourceBuffer[(m + k) * elementsPerPacket + packetIndex] = localData[localDataIndex] * factor
                * tables[dimension - 1][t];
                t++;
                localDataIndex++;
            }
            packetIndex++;
        }
    }
}

void unpack(complex double* localData, complex double* packet, int packetDimensions[], int localDataDimensions[],
                    long localDataIndex, long packetIndex, int depth, int dimension)
{
    // Recursive part
    if (depth < dimension - 1) {
        long localDataStride = 1;
        long packetStride = 1;
        for (int l = depth + 1; l < dimension; l++) {
            localDataStride *= localDataDimensions[l];
            packetStride *= packetDimensions[l];

        }
        for (int t = 0; t < packetDimensions[depth]; t++) {
            unpack(localData, packet, packetDimensions, localDataDimensions, localDataIndex, packetIndex, depth + 1, dimension);
            localDataIndex += localDataStride;
            packetIndex += packetStride;
        }
    }

    // Iterative part
    else {
        memcpy(localData + localDataIndex, packet + packetIndex, sizeof(complex double) * packetDimensions[dimension - 1]);
    }
}

void bspRedistributeAndTwiddle(complex double* localData, int dimension, int n[], int p[])
{
    // Preperation: allocating memory, etc.
    // ---------------------------------------------------------------------------------------
    long totalNumberOfElements = 1;
    int totalProcessors = 1;
    for (int j = 0; j < dimension; j++) {
        totalNumberOfElements *= n[j]/p[j];
        totalProcessors *= p[j];
    }

    complex double* receiveBuffer = malloc(totalNumberOfElements * sizeof(complex double));
    bsp_push_reg(receiveBuffer, totalNumberOfElements * sizeof(complex double));
    complex double* sourceBuffer = malloc(totalNumberOfElements * sizeof(complex double));

    bsp_sync(); // For registering the receiveBuffer

    int sMulti[dimension];
    int s = bsp_pid();
    flatToCurvy(sMulti, p, dimension, s);

    complex double** tables = malloc(dimension * sizeof(void*));

    int packetDimensions[dimension];
    int localDataDimensions[dimension];
    long elementsPerPackage = 1;

    for (int i = 0; i < dimension; i++) {
        localDataDimensions[i] = n[i]/p[i];
        packetDimensions[i] = localDataDimensions[i]/p[i];
        elementsPerPackage *= packetDimensions[i];
        tables[i] = malloc(n[i]/p[i] * sizeof(complex double));
    }

    tablesInitialization(tables, n, p, sMulti, dimension);


    size_t packetSize = elementsPerPackage * sizeof(complex double);

    int kMulti[dimension];
    int startingPoint[dimension]; // To keep track where we begin writing for the unpacking stage.

    // The packing, twiddling, sending and unpacking of the packets.
    // ---------------------------------------------------------------------------------------
    packAndTwiddle(localData, sourceBuffer, n, p, 0, 0, 0, 0, dimension, 1, tables, elementsPerPackage);

    for (int k = 0; k < totalProcessors; k++) {
        bsp_put(k, sourceBuffer + k * elementsPerPackage, receiveBuffer, s * packetSize, packetSize);
    }

    bsp_sync();

    // Unpacking
    for (int k = 0; k < bsp_nprocs(); k++) {
        flatToCurvy(kMulti, p, dimension, k);
        for (int i = 0; i < dimension; i++) {
            startingPoint[i] = kMulti[i] * packetDimensions[i];
        }
        unpack(localData, receiveBuffer + k * elementsPerPackage, packetDimensions, localDataDimensions, curvyToFlat(startingPoint, localDataDimensions, dimension),
        0, 0, dimension);
    }

    // Cleanup
    // ----------------------------------------------------------------------------------------
    bsp_pop_reg(receiveBuffer);
    free(receiveBuffer);
    free(sourceBuffer);
    for (int i = 0; i < dimension; i++) {
        free(tables[i]);
    }
    free(tables);
}

void bspFft(complex double* localData, int dimension, int n[], int p[], fftw_plan bigPlan, fftw_plan manyPlans)
{
    fftw_execute(bigPlan);

    bspRedistributeAndTwiddle(localData, dimension, n, p);

    fftw_execute(manyPlans);
}

// ---------------------------------------------------------------------------------------------------------------
// We have now fully implemented the parallel fft function bspFft. Creating the plans bspFft takes as argument,
// and determining the number of processors you can use is not trivial. So below is some code taking care of this.
// Hopefully this makes using bspFft less painful.

fftw_plan createManyPlans(complex double* localData, int dimension, int n[], int p[], unsigned flag)
{
    bool validInput = true;
    for (int i = 0; i < dimension; i++) {
        if (n[i] % (p[i] * p[i]) != 0) {
            validInput = false;
        }
    }

    if (!validInput) {
        printf("\nPlease make sure that p_i^2 divides n_i in every dimension.\n");
    }

    fftw_iodim* sizes = malloc(dimension * sizeof(fftw_iodim));
    fftw_iodim* startingPoints = malloc(dimension * sizeof(fftw_iodim));

    long strideFactor = 1; // will be updated in the loop, at iteration i it should be
                           // n[i + 1] ... n[dimension - 1] (empty product is 1)
    for (int i = dimension - 1; i >= 0; i--) {
        sizes[i].n = p[i];
        sizes[i].is = n[i]/p[i]/p[i] * strideFactor;
        sizes[i].os = sizes[i].is;

        startingPoints[i].n = n[i]/p[i]/p[i];
        startingPoints[i].is = strideFactor;
        startingPoints[i].os = startingPoints[i].is;

        strideFactor *= n[i]/p[i];
    }

    fftw_plan manyPlans = fftw_plan_guru_dft(dimension, sizes, dimension, startingPoints, localData, localData, FFTW_FORWARD, flag);

    free(sizes);
    free(startingPoints);

    return manyPlans;
}

fftw_plan createBigPlan(complex double* localData, int dimension, int n[], int p[], unsigned flag)
{
    bool validInput = true;
    for (int i = 0; i < dimension; i++) {
        if (n[i] % (p[i] * p[i]) != 0) {
            validInput = false;
        }
    }

    if (!validInput) {
        printf("\nPlease make sure that p_i^2 divides n_i in every dimension.\n");
    }

    int* np = malloc(dimension * sizeof(int));
    for (int i = 0; i < dimension; i++) {
        np[i] = n[i]/p[i];
    }
    fftw_plan plan = fftw_plan_dft(dimension, np, localData, localData, FFTW_FORWARD, flag);
    free(np);
    return plan;
}

int largestSquareDivisor(int n, int p)
{
    int largestSquareDivisor = 1;
    int candidate = 1;

    while (candidate * candidate <= n) {
        if ((n % (candidate * candidate) == 0) && (p % candidate == 0)) {
            largestSquareDivisor = candidate;
        }
        candidate++;
    }

    return largestSquareDivisor;
}

bool checkAdmissable(int n[], int p, int d)
{
    for (int l = 0; l < d; l++) {
        p /= largestSquareDivisor(n[l], p);
    }

    return p == 1;
}

bool fillProcessors(int n[], int p, int* pMulti, int d)
{
    for (int l = 0; l < d; l++) {
        pMulti[l] = largestSquareDivisor(n[l], p);
        p /= pMulti[l];
    }

    return p == 1;
}

void printAdmissableProcessors(int n[], int d)
{
    printf("\n");

    long nTotal = 1;
    for (int i = 0; i < d; i++) {
        nTotal *= n[i];
    }

    // if p factorizes like that, p^2 <= n1 ... nd
    for (int p = 0; p <= sqrt(nTotal); p++) {
        if (checkAdmissable(n, p, d)) {
            printf("%d, ", p);
        }
    }

    printf("\n");
}
