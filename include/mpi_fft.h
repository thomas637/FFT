#ifndef FFT_BSP_MPI_H
#define FFT_BSP_MPI_H
#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <fftw3.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>
#include <mpi.h>

typedef struct {
    fftw_plan bigPlan;
    fftw_plan manyPlans;
} fftu_plan;

/*
Takes a dimension-dimensional tuple indices, and returns the row-major index
with respect to an array of size sizes.
*/
int curvyToFlat(int* indices, int* sizes, int dimension);

/*
If flatIndex is the row-major index with respect to a dimension-dimensional array
sizes, initializes indices with the normal index.
*/
void flatToCurvy(int* indices, int* sizes, int dimension, int flatIndex);

/*
Initializes array table with omega_{n}^{sk} for 0 <= k < n/p and
omega_n the primitive nth root of unity.
*/
void tableInitialization(complex double* table, int n, int p, int s);

/*
Does the previous function for dimension tables.
*/
void tablesInitialization(complex double** tables, int n[], int p[], int s[], int dimension);

/*
Recursive function that initializes the rdispls argument of MPI_Alltoallv. Starts with index = 0,
depth = 0, sum = 0.
*/
void displacementInit(int* rdispls, int dimension, int n[], int p[], int index, int depth, int sum);

/*
Recursive function which twiddles localData (step 2 of the 4-step framework), and packs it in a contiguous
buffer sourceBuffer. Dividing into |S| equal parts, the kth part gets send to the kth process. The call starts
with localDataIndex = packetIndex = depth = m = 0 and factor = 1.
*/
void packAndTwiddle(complex double* localData, complex double* sourceBuffer, int n[], int p[],
                    int localDataIndex, int packetIndex, int m, int depth, int dimension, complex double factor,
                    complex double** tables, int elementsPerPacket);

/*
Executes step 3 of the 4-step framework (the data movement).
*/
void mpiRedistributeAndTwiddle(complex double* localData, int dimension, int n[], int p[]);

/*
The parallel FFT function. localData is the data, of dimension dimension and size n. p is the processor grid,
it must satisfy p[i]^2 | n[i] and p[0] * ... * p[dimension - 1] must equal the number of available processors.
bigPlan is a plan to calculate step 1, and manyPlans is the plan to calculate step 4 of the 4-step framework
(FFTW can do all the small FFTs of the last step with one plan).
*/
void fftu(complex double* localData, int dimension, int n[], int p[], fftu_plan plan);

// ---------------------------------------------------------------------------------------------------------------
// We have now fully implemented the parallel fft function fftu. Creating the plans fftu takes as argument,
// and determining the number of processors you can use is not trivial. So below is some code taking care of this.
// Hopefully this makes using fftu less painful.

/*
The following two functions are for initializing fftu.
*/
fftu_plan createPlan(complex double* localData, int dimension, int n[], int p[], unsigned flag);
fftw_plan createManyPlans(complex double* localData, int dimension, int n[], int p[], unsigned flag);
fftw_plan createBigPlan(complex double* localData, int dimension, int n[], int p[], unsigned flag);
void fftu_destroy_plan(fftu_plan plan);

/*
Returns the largest k such that k^2 | n and p | k
*/
int largestSquareDivisor(int n, int p);

/*
Checks whether p decomposes as p[0], ..., p[d - 1] such that p[i]^2 | n[i].
*/
bool checkAdmissable(int n[], int p, int d);

/*
If p decomposes as in the following function, it returns true and fills
pMulti with that decomposition. Otherwise it returns false.
*/
bool fillProcessors(int n[], int p, int* pMulti, int d);

/*
Allocates a local buffer for a d-dimensional array of size n[0] x ... x n[d - 1]
over a processor grid of p[0] x ... x p[d - 1]. Returns the number of elements allocated. 
*/
int fftu_malloc(int n[], int p[], int d, complex double **localElements);

#endif
