#ifndef FFT_BSP_BSPLIB_H
#define FFTW_BSP_BSPLIB_H
#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <fftw3.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include <bsp.h>

/*
Returns the row-major index corresponding to incidices
with respect to a dimension-dimensional array of size size.
*/
long curvyToFlat(int* indices, int* sizes, int dimension);

/*
Initializes indices to the multidimensional index corresponding to the
row-major index flatIndex, relative to a multidimensional array of size
size[0] x ... x size[dimension - 1].
*/
void flatToCurvy(int* indices, int* sizes, int dimension, int flatIndex);

/*
Processor s multiplies localData[k] by omega_{n_1}^{s_1k_1} \cdots \omega_{n_d}^{s_dk_d},
and then sends localData[k : p : n/p] (strided subarray starting at k, with stride p_l in the lth
dimension) to P(k) and stores it as the subarray localData[sn/p^2 : (s + 1)n/p^2 - 1].
*/
void bspRedistributeAndTwiddle(complex double* localData, int dimension, int n[], int p[]);

/*
The actual FFT function. Takes localData, which is the input of size n distributed multidimensional cyclic
over p[0] ... p[dimension - 1] processors, stored in row-major format. Both plan and manyPlans are DFT plans
on localData. plan is the normal DFT, manyPlans is p[0] ... p[dimensions - 1] DFTs of size n/p^2 distributed
multidimensional cyclic within localData. When this function is finished, localData contains the transformed input.
*/
void bspFft(complex double* localData, int dimension, int n[], int p[], fftw_plan bigPlan, fftw_plan manyPlans);

// ---------------------------------------------------------------------------------------------------------------
// Convenience functions


/* The following two functions create the plans used in bspFft, where flag is FFTW_ESTIMATE, FFTW_MEASURE, etc. */
fftw_plan createManyPlans(complex double* localData, int dimension, int n[], int p[], unsigned flag);
fftw_plan createBigPlan(complex double* localData, int dimension, int n[], int p[], unsigned flag);

/* Returns the largest integer a such that a^2 | n and a | p */
int largestSquareDivisor(int n, int p);

/*
Checks whether p can be factorised as p1 ... pd such that
pl^2 | nl
*/
bool checkAdmissable(int n[], int p, int d);

/*
Factorises p as p1...pd such that pl^2 | nl and stores
it in pMulti if possible.
Returns true if this is possible, and false otherwise.
*/
bool fillProcessors(int n[], int p, int* pMulti, int d);

/*
Prints all numbers p that factorise as p1 ... pd where
pl^2 | nl
*/
void printAdmissableProcessors(int n[], int d);

#endif
