CC = gcc
MPICC = mpicc
BSPCC = bspcc

FLAGS = -O3 -march=native -mtune=native -ffast-math -Wall -Iinclude
LFLAGS_MPI = -lpfft -lfftw3_mpi -lfftw3 -lm
LFLAGS_BSP = -lfftw3_threads -lfftw3 -lm -pthread
LFLAGS_SEQ = -lfftw3_threads -lfftw3 -lm -pthread

MPI_SRC = $(wildcard apps/mpi/*.c)
BSP_SRC = $(wildcard apps/bsp/*.c)
SEQ_SRC = $(wildcard apps/seq/*.c)

MPI_BIN = $(patsubst apps/mpi/%.c, bin/%_mpi, $(MPI_SRC))
BSP_BIN = $(patsubst apps/bsp/%.c, bin/%_bsp, $(BSP_SRC))
SEQ_BIN = $(patsubst apps/seq/%.c, bin/%_seq, $(SEQ_SRC))

all: mpi bsp seq

mpi: $(MPI_BIN)
bsp: $(BSP_BIN)
seq: $(SEQ_BIN)

bin/mpi_lib.o: src/mpi_fft.c
	$(MPICC) $(FLAGS) -c $< -o $@

bin/bsp_lib.o: src/mpi_fft.c
	$(BSPCC) $(FLAGS) -c $< -o $@

bin/%_mpi: apps/mpi/%.c bin/mpi_lib.o
	$(MPICC) $(FLAGS) $^ -o $@ $(LFLAGS_MPI)

bin/%_bsp: apps/bsp/%.c bin/bsp_lib.o
	$(MPICC) $(FLAGS) $^ -o $@ $(LFLAGS_BSP)

bin/%_seq: apps/seq/%.c
	$(MPICC) $(FLAGS) $^ -o $@ $(LFLAGS_SEQ)

clean:
	$(RM) bin/*
