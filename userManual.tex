\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{adjustbox}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{fullpage}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{multicol}
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\usepackage{subcaption}

\DeclareMathOperator{\divi}{div}
\DeclareMathOperator{\md}{~{\mathrm{mod}}~}

\setlength{\parindent}{0pt}
\pgfplotsset{compat=1.17}
    
\title{User manual for FFTU}

\begin{document}

\maketitle

\section{Introduction}

FFTU is a small library for computing Fourier Transforms in arbitrary dimension
on distributed systems. It is built on FFTW \cite{FFTW} and MPI. It 
provides a cyclic distribution to complement FFTW's slab distribution. 
The routine starts and ends in the same distribution, while only communicating
any element once. It also scales to $O\left(\sqrt{n}\right)$ processors 
regardless of the shape of the input. The algorithm and some runtime 
measurements are detailed in \cite{fftu} (arXiv version available).

\section{Installation}

As the code is only a few hundred lines, it is easiest to compile 
\texttt{mpi\_fft.c} with your application. On a Unix-like system, this
looks like

\texttt{mpicc application.c mpi\_fft.c -o application -lfftw3 -lm}

\section{Data distribution}

This routine uses a $n$-dimensional cyclic distribution. That means that for
an array of size $n_1 \times \cdots \times n_d$ and a processor grid of 
$p_1 \times \cdots \times p_d$, we map global index $(i_1, \cdots, i_d)$ 
to local index $(i_1 \divi p_1, \cdots, i_d \divi p_d)$ on processor 
$(i_1 \md p_1, \cdots, i_d \md p_d)$. We assume $p_l^2 \ | \ n_l$.

\section{Usage}

Let us go through an example in order to explain usage of the library.
This application is run as \texttt{mpirun -n <p> ./application <n1> ... <nd>}
and will compute the complex Fourier Transform on a $n_1 \times \cdots n_d$
array on $p$ processors.

First we set up the MPI environment as in a normal MPI program, and
parse the commandline arguments. 

\begin{lstlisting}
int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int dimension = argc - 1; // argv[0] is the program name itself
    int n[dimension], p[dimension];
    int totalProcessors;
    MPI_Comm_size(MPI_COMM_WORLD, &totalProcessors);

    for (int i = 0; i < dimension; i++) {
        n[i] = atoi(argv[i + 1]);
    }
\end{lstlisting}

Now we need to distribute our processors over a $d$-dimensional grid.
A function 
\texttt{bool fillProcessors(int n, int totalProcessors, int p[dimension], int dimension}
is provided. It returns whether the condition $p_l^2 \ | \ n_l$ can be satisfied. 

\begin{lstlisting}
    if (!fillProcessors(n, totalProcessors, p, dimension)) {
        printf("\nIncompatible number of processors.\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
\end{lstlisting}

Then as in an FFTW program, we need to do some planning, and make sure our
array \texttt{localData} is allocated with a special allocator. This function
\texttt{fftu\_plan createPlan(complex double *localData, int dimension, int n[], int p[], unsigned flag)} takes the same planner flags as FFTW.

\begin{lstlisting}
    complex double *localData;
    int totalElements = fftu_malloc(n, p, dimension, &localData);
    init(localData, totalElements);

    fftu_plan plan = createPlan(localData, dimension, n, p, FFTW_MEASURE);
\end{lstlisting}

Then all that is needed is calling \texttt{fftu}.

\begin{lstlisting}
    fftu(localData, dimension, n, p, plan);

    MPI_Finalize();
}
\end{lstlisting}

\bibliographystyle{plain}
\bibliography{refs.bib}

\end{document}
