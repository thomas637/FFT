This code implements a distributed algorithm for the higher-dimensional FFT, building on FFTW for the sequential parts. 
The algorithm is described in the paper "Minimizing communication in the Multidimensional FFT" (https://doi.org/10.1137/22M1487242) (note that in the paper we number from 1, but in the code we start from 0 so Dijkstra does not get mad). 

# Usage

See UserManual.pdf

# Contents of this repository

This was developed as a master thesis using BSPlib, and only later an MPI version was written
to squeeze some extra performance. For this reason both versions are supplied. 

## src

`src/mpi_fft.c` - MPI implementation, should be used in production  

`src/fft.c` - BSPlib implementation  

## include

`include/mpi_fft.h` - Header for MPI implementation  

`include/fft.h` - Header for BSPlib implementation  

## apps

Example applications and code used to obtain the timings from the paper.

### apps/seq

`apps/seq/bench.c` - Program to bench sequential FFTW.  

`apps/seq/testFileGenerator.c` - Creates random arrays, used for testing  

`apps/distributer.c` - Splits the files created by `testFileGenerator.c` into one for each processor.  

### apps/mpi

`apps/mpi/bench.c` - Benchmark code for FFTU  

`apps/mpi/fftw.c` - Benchmark code for FFTW  

`apps/mpi/test.c` - Code to test the implementation of FFTU is correct  

`apps/mpi/pip.c` - Benchmark code for PFFT  

`apps/mpi/minimal.c` - Minimal example explained in the user manual.  

### apps/bsp

`apps/bsp/bench.c` - Benchmark code for FFTU, BSPlib version  

`apps/bsp/test.c` - Code to test the implementation of FFTU is correct, BSPlib version  

# FFTU 'vs' FFTW

This really should be seen as an additional option to FFTW's distributed
plan, rather than a new software system.

This is an implemention of a new algorithm, that works on a different distribution.
For very rectangular arrays, it scales to more processors, and the start and end distribution
is the same. Whether you need this, depends on your application.

It would be nice if this algorithm could be incorporated into FFTW, but I do
not have the skills or time to do this on my own. If someone is interested in doing this 
though, I am happy to help. Send me a line at `thomas.koopman@ru.nl`.
