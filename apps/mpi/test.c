#include <mpi_fft.h>

// At this location, we need .txt files of the random input,
// sequentially calculated output, and there distributed variants
// in the format described in distributer.c and fftwtest.c, which can also be
// used to generated these files.
#define PATH "/home/thomas/Documents/thesis/FFT/test/testfiles/"

// Fills localData with the input for s
void initialise(complex double* localData, int s, int length)
{
    char inputName[250];
    strcpy(inputName, PATH);
    strcat(inputName, "input");
    char suffix[20];
    snprintf(suffix, 20, "%d.txt", s);
    strcat(inputName, suffix);
    FILE* input = fopen(inputName, "r");

    for (int j = 0; j < length; j++) {
        double real, compl;
        int error = fscanf(input, "%lf,%lf,", &real, &compl);
        if (error < 0) {
            printf("\nScanning went wrong\n");
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
        localData[j] = real + compl * I;
    }

    fclose(input);
}

// Returns true if localData is pointwise within error of outputs1_s2.txt
bool equalityTester(complex double* localData, int s, int length, double error)
{
    bool equal = true;

    char outputName[250];
    strcpy(outputName, PATH);
    strcat(outputName, "output");
    char suffix[20];
    snprintf(suffix, 20, "%d.txt", s);
    strcat(outputName, suffix);
    FILE* output = fopen(outputName, "r");

    for (int j = 0; j < length; j++) {
        double real, compl;
        int error = fscanf(output, "%lf,%lf,", &real, &compl);

        if (error < 0) {
            printf("\nScanning went wrong\n");
            MPI_Abort(MPI_COMM_WORLD, 1);
        }

        complex double realValue = real + compl * I;

        if (cabs(realValue - localData[j]) > error) {
            equal = false;
        }
    }

    fclose(output);

    return equal;
}

// Tests the bspFft function on the testfile input
void mpiTest()
{
    int dimension = 4;
    int n[] = {256, 9, 36, 4};
    int p[] = {2, 3, 1, 1};

    int s;
    MPI_Comm_rank(MPI_COMM_WORLD, &s);

    int numberOfElements = 1;
    for (int i = 0; i < dimension; i++) {
        numberOfElements *= n[i]/p[i];
    }
    complex double* localData = fftw_malloc(numberOfElements * sizeof(complex double));
    if (localData == NULL) {
        MPI_Abort(MPI_COMM_WORLD, 2);
    }

    // Create plans
    fftu_plan plan = createPlan(localData, dimension, n, p, FFTW_ESTIMATE);

    if (plan.bigPlan == NULL || plan.manyPlans == NULL) {
        MPI_Abort(MPI_COMM_WORLD, 3);
    }

    initialise(localData, s, numberOfElements);

    fftu(localData, dimension, n, p, plan);

    bool success = equalityTester(localData, s, numberOfElements, 0.001);

    if (success) {
        printf("\nSucces from P(%d)\n", s);
    } else {
        printf("\nFailure from  P(%d)\n", s);
    }

    fftw_free(localData);
    fftu_destroy_plan(plan);
}

// For profiling the function
void mpiTime()
{
    int dimension = 5;
    int n[] = {64, 32, 32, 32, 32}; // 2^26
    int p[] = {4, 1, 1, 1, 1};

    int numberOfElements = 1;
    for (int i = 0; i < dimension; i++) {
        numberOfElements *= n[i]/p[i];
    }
    complex double* localData = fftw_malloc(numberOfElements * sizeof(complex double));
    if (localData == NULL) {
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    // Create plan
    fftu_plan plan = createPlan(localData, dimension, n, p, FFTW_ESTIMATE);

    if (plan.bigPlan == NULL || plan.manyPlans == NULL) {
        MPI_Abort(MPI_COMM_WORLD, 2);
    }

    for (int i = 0; i < numberOfElements; i++) {
        localData[i] = 1.0 + 1.0 * I;
    }

    MPI_Barrier(MPI_COMM_WORLD);

    double time1 = MPI_Wtime();

    fftu(localData, dimension, n, p, plan);

    MPI_Barrier(MPI_COMM_WORLD);

    double time2 = MPI_Wtime();

    int s;

    MPI_Comm_rank(MPI_COMM_WORLD, &s);

    if (s == 0) {
        printf("\nIt took %lf ms. in total.\n", 1000 * (time2 - time1));
    }

    fftw_free(localData);
    fftu_destroy_plan(plan);
}

int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);

	mpiTest();

    MPI_Finalize();

    return 0;
}
