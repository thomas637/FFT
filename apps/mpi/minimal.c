#include <mpi_fft.h>
#include <complex.h>

void init(complex double *localData, int totalElements)
{
    for (int i = 0; i < totalElements; i++) {
        localData[i] = 1.0 + 1.0 * I;
    }
}

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int dimension = argc - 1; // argv[0] is the program name itself
    int n[dimension], p[dimension];
    int totalProcessors;
    MPI_Comm_size(MPI_COMM_WORLD, &totalProcessors);

    for (int i = 0; i < dimension; i++) {
        n[i] = atoi(argv[i + 1]);
    }

    if (!fillProcessors(n, totalProcessors, p, dimension)) {
        printf("\nIncompatible number of processors.\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    complex double *localData;
    int totalElements = fftu_malloc(n, p, dimension, &localData);
    init(localData, totalElements);

    fftu_plan plan = createPlan(localData, dimension, n, p, FFTW_MEASURE);

    fftu(localData, dimension, n, p, plan);

    MPI_Finalize();
}
