#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <fftw3-mpi.h>

#define NITERS 100 

/* Runs the fftw version NITERS times and prints average running time. */
void fftwBench(int argc, char** argv)
{
    MPI_Init(&argc, &argv);
	
    int dimension = argc - 2;
    ptrdiff_t n[dimension];
    int p = atoi(argv[argc - 1]);
    
    for (int i = 0; i < dimension; i++) {
        n[i] = atoi(argv[i + 1]);
    }
    
    ptrdiff_t totalElements = 1;
    for (int j = 0; j < dimension; j++) {
        totalElements *= n[j];
    }

    fftw_plan plan, plan2;
    complex double* data;
    ptrdiff_t alloc_local, local_n0, local_0_start, i;

    fftw_mpi_init();

    alloc_local = fftw_mpi_local_size_many(dimension, n, 1, FFTW_MPI_DEFAULT_BLOCK,
    MPI_COMM_WORLD, &local_n0, &local_0_start);

    data = fftw_alloc_complex(alloc_local);

    plan = fftw_mpi_plan_many_dft(dimension, n, 1, FFTW_MPI_DEFAULT_BLOCK,
    FFTW_MPI_DEFAULT_BLOCK, data, data, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE);

    plan2 = fftw_mpi_plan_many_dft(dimension, n, 1, FFTW_MPI_DEFAULT_BLOCK,
    FFTW_MPI_DEFAULT_BLOCK, data, data, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE | FFTW_MPI_TRANSPOSED_OUT);
    
    for (i = 0; i < totalElements / p; i++) {
        data[i] = 1.0 + 1.0 * I;
    }

    MPI_Barrier(MPI_COMM_WORLD);
    double time1 = MPI_Wtime();

    for (int i = 0; i < NITERS; i++) {
        fftw_execute(plan);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    double time2 = MPI_Wtime();

    for (int i = 0; i < NITERS; i++) {
        fftw_execute(plan2);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    double time3 = MPI_Wtime();

    int s;
    MPI_Comm_rank(MPI_COMM_WORLD, &s);
    if (s == 0) {
        printf("%d,%d,%lf,%lf\n", dimension, p, 1000 * (time2 - time1) / NITERS,
		1000 * (time3 - time2) / NITERS);
    }

    fftw_destroy_plan(plan);
    fftw_destroy_plan(plan2);

    fftw_free(data);

    MPI_Finalize();
}


int main(int argc, char **argv)
{
    fftwBench(argc, argv);
}
