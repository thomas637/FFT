#include <pfft.h> 
#include <stdlib.h>

#define NITERS 100 

/* Command line arguments are first the dimensions, then finally
   the number of processors to use. */
int main(int argc, char **argv){
    int dimension = argc - 2;
    int np[2]; // processor distribution, 2 dimensions suffices for our experiments.
    ptrdiff_t n[dimension];
    int totalProcessors = atoi(argv[argc - 1]);

    ptrdiff_t local_ni[dimension], local_i_start[dimension];
    ptrdiff_t local_no[dimension], local_o_start[dimension];
    ptrdiff_t alloc_local;
    pfft_complex* data;
    pfft_plan plan=NULL;
    pfft_plan plan2=NULL;
    MPI_Comm comm_cart_2d;

    /* Set size of FFT and process mesh */
    for (int i = 0; i < dimension; i++) {
        n[i] = atoi(argv[i + 1]);
    }

    np[0] = (totalProcessors < n[0]) ? totalProcessors : n[0];
    np[1] = (totalProcessors < n[0]) ? 1 : totalProcessors / n[0];

    /* Initialize MPI and PFFT */
    MPI_Init(&argc, &argv);
    pfft_init();

    /* Create two-dimensional process grid of size np[0] x np[1] */
    pfft_create_procmesh_2d(MPI_COMM_WORLD, np[0], np[1],
    &comm_cart_2d);

    /* Get parameters of data distribution */
    alloc_local = pfft_local_size_dft(dimension, n, comm_cart_2d,
        PFFT_TRANSPOSED_OUT, local_ni, local_i_start, // change PFFT_TRANSPOSED_OUT to PFFT_TRANSPOSED_NONE 
        local_no, local_o_start);

    /* Allocate memory */
    data = pfft_alloc_complex(alloc_local);

    /* Plan parallel forward FFT */
    plan = pfft_plan_dft(dimension, n, data, data, comm_cart_2d, 
    PFFT_FORWARD, PFFT_TRANSPOSED_NONE); 

    plan2 = pfft_plan_dft(dimension, n, data, data, comm_cart_2d, 
    PFFT_FORWARD, PFFT_TRANSPOSED_OUT); 
    
    /* Execute parallel forward FFT */
    MPI_Barrier(MPI_COMM_WORLD);
    double time1 = MPI_Wtime();

    for (int i = 0; i < NITERS; i++) {
        pfft_execute(plan);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    double time2 = MPI_Wtime();

    for (int i = 0; i < NITERS; i++) {
        pfft_execute(plan2);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    double time3 = MPI_Wtime();

    int s;
    MPI_Comm_rank(MPI_COMM_WORLD, &s);
    if (s == 0) {
        printf("%d,%d,%lf,%lf\n", dimension, totalProcessors, 1000 * (time2 - time1) / NITERS,
	1000 * (time3 - time2) / NITERS);
    }

    /* free mem and finalize MPI */
    pfft_destroy_plan(plan);
    pfft_destroy_plan(plan2);
    MPI_Comm_free(&comm_cart_2d);
    pfft_free(data);
    MPI_Finalize();
    return 0;
}
