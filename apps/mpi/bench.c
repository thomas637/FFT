#include <mpi_fft.h>
#include <complex.h>

#define NITERS 100

/* Runs the bsp version NITERS times and prints average running time.
   Input the array sizes first, then the number of processors. So
   for an array 1024 x 1024 x 1024 on 4 processors, we would supply
   command line arguments 1024 1024 1024 4. */
void bspBench(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int dimension = argc - 2; // argv[0] is the program name itself
    int n[dimension], p[dimension];
    int totalProcessors = atoi(argv[argc - 1]);

    for (int i = 0; i < dimension; i++) {
        n[i] = atoi(argv[i + 1]);
    }


    if (!fillProcessors(n, totalProcessors, p, dimension)) {
        printf("\nIncompatible number of processors.\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    int totalElements = 1;
    for (int i = 0; i < dimension; i++) {
        totalElements *= n[i]/p[i];
    }

    complex double* localData = fftw_malloc(totalElements * sizeof(complex double));

    fftu_plan plan = createPlan(localData, dimension, n, p, FFTW_MEASURE);

    for (int i = 0; i < totalElements; i++) {
        localData[i] = 1.0 + 1.0 * I;
    }

    MPI_Barrier(MPI_COMM_WORLD);
    double time1 = MPI_Wtime();

    for (int i = 0; i < NITERS; i++) {
        fftu(localData, dimension, n, p, plan);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    double time2 = MPI_Wtime();

    int s;
    MPI_Comm_rank(MPI_COMM_WORLD, &s);
    if (s == 0) {
        printf("%d,%d,%lf\n", dimension, totalProcessors, 1000 * (time2 - time1) / NITERS);
    }

    MPI_Finalize();
}

int main(int argc, char **argv)
{
    bspBench(argc, argv);
}
