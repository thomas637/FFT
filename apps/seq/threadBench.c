#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <fftw3.h>
#include <time.h>

#define NITERS 5

/* Runs the fftw version NITERS times and prints average running time. */
void fftwBench(int argc, char** argv)
{
    int dimension = argc - 2;
    int n[dimension];

    for (int i = 0; i < dimension; i++) {
        n[i] = atoi(argv[i + 1]);
    }

    int totalElements = 1;
    for (int j = 0; j < dimension; j++) {
        totalElements *= n[j];
    }
    int numberOfThreads = atoi(argv[argc - 1]);
    if (!fftw_init_threads()) {
        fprintf(stderr, "Thread initalisation unsuccessfull.\n");
        exit(EXIT_FAILURE);
    }
    fftw_plan_with_nthreads(numberOfThreads);

    complex double* data = fftw_malloc(totalElements * sizeof(complex double));

    fftw_plan plan = fftw_plan_dft(dimension, n, data, data, FFTW_FORWARD,
        FFTW_MEASURE);

    for (int i = 0; i < totalElements; i++) {
        data[i] = 1.0 + 1.0 * I;
    }

    clock_t time1 = clock();

    for (int i = 0; i < NITERS; i++) {
        fftw_execute(plan);
    }

    clock_t time2 = clock();

    printf("Transforming an array of size ");
    for (int i = 0; i < dimension - 1; i++) {
        printf("%d x ", n[i]);
    }

    printf("%d using FFTW on %d threads took %lf ms. on average.\n", n[dimension - 1],
            numberOfThreads, 1000.0 * (time2 - time1) / NITERS / CLOCKS_PER_SEC);

    fftw_destroy_plan(plan);

    fftw_free(data);
}


int main(int argc, char **argv)
{
    fftwBench(argc, argv);
    exit(EXIT_SUCCESS);
}
