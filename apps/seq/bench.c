#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <fftw3.h>
#include <time.h>

#define NITERS 5

/* Runs the fftw version NITERS times and prints average running time. */
void fftwBench(int argc, char** argv, unsigned flag)
{
    int dimension = argc - 1;
    int n[dimension];

    for (int i = 0; i < dimension; i++) {
        n[i] = atoi(argv[i + 1]);
    }

    int totalElements = 1;
    for (int j = 0; j < dimension; j++) {
        totalElements *= n[j];
    }

    complex double* data = fftw_malloc(totalElements * sizeof(complex double));

    fftw_plan plan = fftw_plan_dft(dimension, n, data, data, FFTW_FORWARD,
        flag);

    for (int i = 0; i < totalElements; i++) {
        data[i] = 1.0 + 1.0 * I;
    }

    clock_t time1 = clock();

    for (int i = 0; i < NITERS; i++) {
        fftw_execute(plan);
    }

    clock_t time2 = clock();

    printf("Transforming an array of size ");
    for (int i = 0; i < dimension - 1; i++) {
        printf("%d x ", n[i]);
    }

    printf("%d using FFTW sequentially took %lf ms. on average.\n", n[dimension - 1], 1000.0 * (time2 - time1) / NITERS / CLOCKS_PER_SEC);

    fftw_destroy_plan(plan);

    fftw_free(data);
}


int main(int argc, char **argv)
{
    printf("FFTW_ESTIMATE\n");
    fftwBench(argc, argv, FFTW_ESTIMATE);
    printf("FFTW_MEASURE\n");
    fftwBench(argc, argv, FFTW_MEASURE);
    printf("FFTW_PATIENT\n");
    fftwBench(argc, argv, FFTW_PATIENT);
}
