/*
File format of a n1 x n2 x ... x nd matrix is in
row-major format, seperated by comma's and no spaces.
We but the real and imaginary components in
that order. So we save

1 + 2i | 3 + 4i
5 + 6i | 7 + 8i

as

1,2,3,4,5,6,7,8,

A file input.txt needs to be saved in the specified directory,
the input for P(s1, ..., sd) will be saved as inputs.txt, where
s is the row-major index corresponding to (s1, ..., sd)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

// The location on your computer where input.txt or output.txt can be found, and
// where we put the distributed files.
#define PATH "/home/thomas/Documents/thesis/FFT/test/testfiles/"

long curvyToFlat(int* indices, int* sizes, int dimension)
{
    long rowMajorIndex = 0;

    // By distributivity of multiplication over addition,
    // we have a sum of all the indices, where
    // indices[i] gets multiplied by size[j] for i < j < dimension
    // so this is correct.
    for (int i = 0; i < dimension; i++) {
        rowMajorIndex *= sizes[i];
        rowMajorIndex += indices[i];
    }

    return rowMajorIndex;
}

void flatToCurvy(int* indices, int* sizes, int dimension, int flatIndex)
{
    for (int i = dimension - 1; i >= 0; i--) {
        indices[i] = flatIndex % sizes[i];
        flatIndex /= sizes[i];
    }
}

// Distributes the input if IO = input and the output if IO = output
void distributer(char IO[], int dimension, int processorNumbers[], int lengths[])
{
    char* fileNameStart = malloc(250 * sizeof(char));

    strcpy(fileNameStart, PATH);
    strcat(fileNameStart, IO); // every file name will start with IO

    long numberOfElements = 1;
    int numberOfProcessors = 1;
    for (int j = 0; j < dimension; j++) {
        numberOfElements *= lengths[j];
        numberOfProcessors *= processorNumbers[j];
    }

    char (*fileNames)[250] = malloc(250 * sizeof(char) * numberOfProcessors);

    for (int s = 0; s < numberOfProcessors; s++) {
        char suffix[20];
        snprintf(suffix, 20, "%d.txt", s);
        strcpy(fileNames[s], fileNameStart);
        strcat(fileNames[s], suffix);
    }

    char* inputFileName = fileNameStart;
    strcat(inputFileName, ".txt");

    // Open the files
    FILE* input = fopen(inputFileName, "r");
    FILE** distributedIOFiles = malloc(sizeof(FILE*) * numberOfProcessors);
    for (int s = 0; s < numberOfProcessors; s++) {
        distributedIOFiles[s] = fopen(fileNames[s], "w");
    }

    // Start the writing
    int* elementMulti = malloc(dimension * sizeof(int));
    int* processorMulti = malloc(dimension * sizeof(int));

    // Going from global row-major index to local row-major index preserves order,
    // so we can iterate from 0 to numberOfElements and just write to the correct file.
    for (int j = 0; j < numberOfElements; j++) {
        double real, compl;
        fscanf(input, "%lf,%lf,", &real, &compl);
        flatToCurvy(elementMulti, lengths, dimension, j);
        for (int l = 0; l < dimension; l++) {
            processorMulti[l] = elementMulti[l] % processorNumbers[l];
        }
        int s = curvyToFlat(processorMulti, processorNumbers, dimension);
        fprintf(distributedIOFiles[s], "%lf,%lf,", real, compl);
    }

    // Close the files again
    fclose(input);
    for (int s = 0; s < numberOfProcessors; s++) {
        fclose(distributedIOFiles[s]);
    }


    free(distributedIOFiles);
    free(fileNameStart);
    free(fileNames);
    free(elementMulti);
    free(processorMulti);
}

int main()
{
    int dimension;
    int lengths[30];
    int processorNumbers[30];

    printf("\nPlease enter the dimension\n");
    scanf("%d", &dimension);

    for (int j = 0; j < dimension; j++) {
        printf("\nPlease enter the length of the %dth dimension:\n", j + 1);
        scanf("%d", lengths + j);
        printf("\nPlease enter the number of processors available for that dimension, the square of this must divide the size of the array in that dimension:\n");
        scanf("%d", processorNumbers + j);
    }

    distributer("input", dimension, processorNumbers, lengths);
    distributer("output", dimension, processorNumbers, lengths);
    return 0;
}
