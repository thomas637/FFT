/*
File format of a n1 x n2 x ... x nd matrix is in
row-major format, seperated by comma's and no spaces.
We but the real and imaginary components in
that order. So we save

1 + 2i | 3 + 4i
5 + 6i | 7 + 8i

as

1,2,3,4,5,6,7,8,

The input is saved as input.txt and the output as output.txt
in the specified directory.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <complex.h>
#include <fftw3.h>

// The location on your computer where input.txt or output.txt will be written
#define PATH "/home/thomas/Documents/thesis/FFT/test/testfiles/"

void writeInput(fftw_complex* in, int numberOfElements)
{
    FILE *input;
    char fileName[250];
    strcpy(fileName, PATH);
    strcat(fileName, "input.txt");
    input = fopen(fileName, "w");

    for (int j = 0; j < numberOfElements; j++) {
        fprintf(input, "%lf", creal(in[j]));
        fprintf(input, "%c", ',');
        fprintf(input, "%lf", cimag(in[j]));
        fprintf(input, "%c", ',');
    }

    fclose(input);
}

void writeOutput(fftw_complex* out, int numberOfElements)
{
    FILE *output;
    char fileName[250];
    strcpy(fileName, PATH);
    strcat(fileName, "output.txt");
    output = fopen(fileName, "w");

    for (int j = 0; j < numberOfElements; j++) {
        fprintf(output, "%lf", creal(out[j]));
        fprintf(output, "%c", ',');
        fprintf(output, "%lf", cimag(out[j]));
        fprintf(output, "%c", ',');
    }

    fclose(output);
}

int main()
{
    // Get inputs
    int d;
    int lengths[30];

    printf("\nOf what dimension do you want your dft to be?\n");

    scanf("%d", &d);

    for (int j = 0; j < d; j++) {
        printf("\nType in the length of the %dth dimension: \n", j + 1);
        scanf("%d", lengths + j);
    }

    // Create FFTW plan
    int* lengthsExact = fftw_malloc(sizeof(int) * d);
    for (int j = 0; j < d; j++) {
        lengthsExact[j] = lengths[j];
    }

    int numberOfElements = 1;

    for (int j = 0; j < d; j++) {
        numberOfElements *= lengths[j];
    }

    fftw_complex* in = fftw_malloc(sizeof(fftw_complex) * numberOfElements);
    fftw_complex* out = fftw_malloc(sizeof(fftw_complex) * numberOfElements);

    if (in == NULL || out == NULL) {
        printf("\nMemory allocation failed\n");
        exit(1);
    }

    fftw_plan plan = fftw_plan_dft(d, lengthsExact, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    if (plan == NULL) {
        printf("\nMaking plan failed\n");
        exit(1);
    }

    // Initialise
    srand(time(NULL));
    for (int j = 0; j < numberOfElements; j++) {
        double real = (double) rand() / RAND_MAX;
        double imag = (double) rand() / RAND_MAX;
        in[j] = real + imag * I;
    }

    // Calculate the DFT
    fftw_execute(plan);

    writeInput(in, numberOfElements);
    writeOutput(out, numberOfElements);

    // Clean up
    fftw_destroy_plan(plan);
    fftw_free(in);
    fftw_free(out);
    return 0;
}
