#include "../include/fft.h"
#include <complex.h>

#define NITERS 100

/* Runs the bsp version NITERS times and prints average running time.
   Enter the array size, and then the number of processors. So if you want to transform an array of 
   size 1024 x 1024 x 1024 on 512 processors, you would give 
   1024 1024 1024 512 as commandline arguments. */
int main(int argc, char **argv)
{
    bsp_begin(atoi(argv[argc - 1]));

    int dimension = argc - 2; // argv[0] is the program name itself
    int n[dimension], p[dimension];
    int totalProcessors = atoi(argv[argc - 1]);
    
    for (int i = 0; i < dimension; i++) {
        n[i] = atoi(argv[i + 1]);
    }
    
    if (!fillProcessors(n, totalProcessors, p, dimension)) {
        bsp_abort("\nIncompatible number of processors.\n");
    }

    int totalElements = 1;
    for (int i = 0; i < dimension; i++) {
        totalElements *= n[i]/p[i];
    }

    complex double* localData = fftw_malloc(totalElements * sizeof(complex double));

    fftw_plan bigPlan = createBigPlan(localData, dimension, n, p, FFTW_MEASURE);
    fftw_plan manyPlans = createManyPlans(localData, dimension, n, p, FFTW_MEASURE);

    for (int i = 0; i < totalElements; i++) {
        localData[i] = 1.0 + 1.0 * I;
    }

    bsp_sync();
    double time1 = bsp_time();

    for (int i = 0; i < NITERS; i++) {
        bspFft(localData, dimension, n, p, bigPlan, manyPlans);
    }

    bsp_sync();
    double time2 = bsp_time();

    int s = bsp_pid();

    if (s == 0) {
        printf("Transforming an array of size ");
        for (int i = 0; i < dimension - 1; i++) {
            printf("%d x ", n[i]);
        }
        printf("%d using BSP/BSPlib on %d processors took %lf ms. on average.\n", n[dimension - 1], totalProcessors, 1000 * (time2 - time1) / NITERS);
    }

    bsp_end();    
    return 0;
}
