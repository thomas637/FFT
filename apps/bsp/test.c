#include <fft.h>

// At this location, we need .txt files of the random input,
// sequentially calculated output, and there distributed variants
// in the format described in distributer.c and fftwtest.c, which can also be
// used to generated these files.
#define PATH "/home/thomas/Documents/thesis/FFT/test/testfiles/"

// Fills localData with the input for s
void initialise(complex double* localData, int s, long length)
{
    char inputName[250];
    strcpy(inputName, PATH);
    strcat(inputName, "input");
    char suffix[20];
    snprintf(suffix, 20, "%d.txt", s);
    strcat(inputName, suffix);
    FILE* input = fopen(inputName, "r");

    for (int j = 0; j < length; j++) {
        double real, compl;
        int error = fscanf(input, "%lf,%lf,", &real, &compl);
        if (error < 0) {
            printf("\nScanning went wrong\n");
            bsp_abort("error");
        }
        localData[j] = real + compl * I;
    }

    fclose(input);
}

// Returns true if localData is pointwise within error of outputs1_s2.txt
bool equalityTester(complex double* localData, int s, long length, double error)
{
    bool equal = true;

    char outputName[250];
    strcpy(outputName, PATH);
    strcat(outputName, "output");
    char suffix[20];
    snprintf(suffix, 20, "%d.txt", s);
    strcat(outputName, suffix);
    FILE* output = fopen(outputName, "r");

    for (int j = 0; j < length; j++) {
        double real, compl;
        int error = fscanf(output, "%lf,%lf,", &real, &compl);

        if (error < 0) {
            printf("\nScanning went wrong\n");
            bsp_abort("error");
        }

        complex double realValue = real + compl * I;

        if (cabs(realValue - localData[j]) > error) {
            equal = false;
        }
    }

    fclose(output);

    return equal;
}

// Tests the bspFft function on the testfile input
void bspTest()
{
    bsp_begin(6);

    int dimension = 4;
    int n[] = {256, 9, 36, 4};
    int p[] = {2, 3, 1, 1};

    long numberOfElements = 1;
    for (int i = 0; i < dimension; i++) {
        numberOfElements *= n[i]/p[i];
    }
    complex double* localData = fftw_malloc(numberOfElements * sizeof(complex double));
    if (localData == NULL) {
        bsp_abort("\nAllocating memory failed.\n");
    }

    // Create plans
    fftw_plan plan = createBigPlan(localData, dimension, n, p, FFTW_ESTIMATE);
    fftw_plan manyPlans = createManyPlans(localData, dimension, n, p, FFTW_ESTIMATE);

    if (plan == NULL || manyPlans == NULL) {
        bsp_abort("\nCreating plans failed\n");
    }

    initialise(localData, bsp_pid(), numberOfElements);

    bspFft(localData, dimension, n, p, plan, manyPlans);

    bool success = equalityTester(localData, bsp_pid(), numberOfElements, 0.001);

    if (success) {
        printf("\nSucces from P(%d)\n", bsp_pid());
    } else {
        printf("\nFailure from  P(%d)\n", bsp_pid());
    }

    fftw_free(localData);
    fftw_destroy_plan(plan);
    fftw_destroy_plan(manyPlans);

    bsp_end();
}

void bspTime()
{
    bsp_begin(4);

    int dimension = 3;
    int n[] = {8, 8, 8};
    int p[] = {2, 2, 1};

    long numberOfElements = 1;
    for (int i = 0; i < dimension; i++) {
        numberOfElements *= n[i]/p[i];
    }
    complex double* localData = fftw_malloc(numberOfElements * sizeof(complex double));
    if (localData == NULL) {
        bsp_abort("\nAllocating memory failed.\n");
    }

    // Create plans
    fftw_plan plan = createBigPlan(localData, dimension, n, p, FFTW_ESTIMATE);
    fftw_plan manyPlans = createManyPlans(localData, dimension, n, p, FFTW_ESTIMATE);

    if (plan == NULL || manyPlans == NULL) {
        bsp_abort("\nCreating plans failed\n");
    }

    for (long i = 0; i < numberOfElements; i++) {
        localData[i] = 1.0 + 1.0 * I;
    }

    bspFft(localData, dimension, n, p, plan, manyPlans);

    fftw_free(localData);
    fftw_destroy_plan(plan);
    fftw_destroy_plan(manyPlans);

    bsp_end();
}

void bspDebug()
{
    bsp_begin(4);

    int dimension = 2;
    int n[] = {8, 4};
    int p[] = {2, 2};

    long numberOfElements = 1;
    for (int i = 0; i < dimension; i++) {
        numberOfElements *= n[i]/p[i];
    }

    complex double* localData = fftw_malloc(numberOfElements * sizeof(complex double));
    if (localData == NULL) {
        bsp_abort("\nAllocating memory failed.\n");
    }

    // Create plans
    fftw_plan plan = createBigPlan(localData, dimension, n, p, FFTW_ESTIMATE);
    fftw_plan manyPlans = createManyPlans(localData, dimension, n, p, FFTW_ESTIMATE);

    if (plan == NULL || manyPlans == NULL) {
        bsp_abort("\nCreating plans failed\n");
    }

    for (long i = 0; i < numberOfElements; i++) {
        localData[i] = 0;
    }

    if (bsp_pid() == 0) {
        localData[0] = 1.0;
    }

    bspFft(localData, dimension, n, p, plan, manyPlans);

    fftw_free(localData);
    fftw_destroy_plan(plan);
    fftw_destroy_plan(manyPlans);

    bsp_end();
}

int main(int argc, char **argv)
{
	bsp_init(bspTest, argc, argv);

	bspTest();

    exit(EXIT_SUCCESS);

    return 0;
}
